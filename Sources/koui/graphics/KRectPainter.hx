package koui.graphics;

import haxe.ds.Vector;

import kha.Canvas;
import kha.FastFloat;
import kha.Shaders;

import koui.theme.Style;

class KRectPainter extends KPainter {
	public function new(g4: kha.graphics4.Graphics, canvas: Canvas) {
		final quadUniformNames = new Vector(QuadUniforms._numUniforms);
		quadUniformNames[QuadUniforms.Rects] = "rects";

		final styleUniformNames = new Vector(StyleUniforms._numUniforms);
		styleUniformNames[StyleUniforms.BorderColors] = "borderColors";
		styleUniformNames[StyleUniforms.ShadowColors] = "shadowColors";
		styleUniformNames[StyleUniforms.Attributes] = "attributes";

		super(g4, canvas, quadUniformNames, styleUniformNames, Shaders.rect_vert, Shaders.rect_frag);
	}

	#if (KOUI_EFFECTS_OFF || KOUI_EFFECTS_SHADOW_OFF)
	private static inline override function init() {}

	@:access(koui.elements.Element)
	public static inline function fillKRect(
			x: FastFloat, y: FastFloat, width: FastFloat, height: FastFloat,
			cornerRadius: Int,
			styleBorder: Dynamic, styleGradient: Dynamic, styleShadow: Dynamic) {
		g2.color = styleGradient.colorBottomRight;
		g2.fillRect(x, y, width, height);
	}
	#else

	@:access(koui.elements.Element)
	public function fillKRect(x: FastFloat, y: FastFloat, width: FastFloat, height: FastFloat, style: Style) {
		// Draw buffer if full
		if (quadIndex + 1 >= KPainter.MAX_QUADS_PER_DRAWCALL || nextFreeStyleIndex + 1 >= KPainter.MAX_STYLES_PER_DRAWCALL) {
			drawBuffer();
		}

		var canvasW = canvas.width;
		var screenH = canvas.height;

		var cornerRadius = style.cornerRadius;
		var styleBorder = style.border;
		var styleColor = style.color;
		var styleShadow = style.dropShadow;
		var shadowWidth = styleShadow.radius;

		// Calculate shadow quad positions in window space
		var posLeft: FastFloat = (x - shadowWidth) / canvasW;
		var posRight: FastFloat = (x + width + shadowWidth) / canvasW;
		// Todo: use Image.renderTargetInvertedY
		#if (kha_opengl || kha_webgl)
		var posTop: FastFloat = ((y - shadowWidth)) / screenH;
		var posBottom: FastFloat = ((y + height + shadowWidth)) / screenH;
		#else
		// Invert y axis
		var posTop: FastFloat = (screenH - (y - shadowWidth)) / screenH;
		var posBottom: FastFloat = (screenH - (y + height + shadowWidth)) / screenH;
		#end

		// Map from [0, 1] to [-1, 1]
		posLeft = posLeft * 2 - 1;
		posRight = posRight * 2 - 1;
		posTop = posTop * 2 - 1;
		posBottom = posBottom * 2 - 1;

		var colorTopLeft = styleColor.bg;
		var colorBottomRight = styleColor.bg;
		var direction = true;
		if (styleColor.useGradient) {
			colorTopLeft = styleColor.gradient.colorTopLeft;
			colorBottomRight = styleColor.gradient.colorBottomRight;
			direction = styleColor.gradient.direction;
		}

		final styleIndex = getStyleIndex(style);

		setRectVertices(posLeft, posRight, posTop, posBottom, styleIndex);
		setRectColors(direction, colorTopLeft, colorBottomRight, style.opacity);

		quadUniformValues[QuadUniforms.Rects][quadIndex * 4 + 0] = x;
		quadUniformValues[QuadUniforms.Rects][quadIndex * 4 + 1] = x + width;
		quadUniformValues[QuadUniforms.Rects][quadIndex * 4 + 2] = y + height;
		quadUniformValues[QuadUniforms.Rects][quadIndex * 4 + 3] = y;

		// Required to implicitly cast the dynamic value to kha.Color
		var borderColor: kha.Color = styleBorder.color;
		styleUniformValues[StyleUniforms.BorderColors][styleIndex * 4 + 0] = borderColor.R;
		styleUniformValues[StyleUniforms.BorderColors][styleIndex * 4 + 1] = borderColor.G;
		styleUniformValues[StyleUniforms.BorderColors][styleIndex * 4 + 2] = borderColor.B;
		styleUniformValues[StyleUniforms.BorderColors][styleIndex * 4 + 3] = borderColor.A * style.opacity;

		var shadowColor: kha.Color = styleShadow.color;
		if (styleShadow.radius == 0) shadowColor = kha.Color.Transparent;
		styleUniformValues[StyleUniforms.ShadowColors][styleIndex * 4 + 0] = shadowColor.R;
		styleUniformValues[StyleUniforms.ShadowColors][styleIndex * 4 + 1] = shadowColor.G;
		styleUniformValues[StyleUniforms.ShadowColors][styleIndex * 4 + 2] = shadowColor.B;
		styleUniformValues[StyleUniforms.ShadowColors][styleIndex * 4 + 3] = shadowColor.A * style.opacity;

		styleUniformValues[StyleUniforms.Attributes][styleIndex * 4 + 0] = styleBorder.size;
		styleUniformValues[StyleUniforms.Attributes][styleIndex * 4 + 1] = cornerRadius;
		styleUniformValues[StyleUniforms.Attributes][styleIndex * 4 + 2] = shadowWidth;
		styleUniformValues[StyleUniforms.Attributes][styleIndex * 4 + 3] = styleShadow.falloff;

		quadIndex++;
	}
	#end
}

private enum abstract QuadUniforms(Int) to Int {
	var Rects;

	var _numUniforms;
}

private enum abstract StyleUniforms(Int) to Int {
	var BorderColors;
	var ShadowColors;
	var Attributes;

	var _numUniforms;
}
