package koui.graphics;

import haxe.ds.Vector;

import kha.Canvas;
import kha.FastFloat;
import kha.Shaders;

import koui.theme.Style;

#if (KOUI_EFFECTS_OFF || KOUI_EFFECTS_SHADOW_OFF)
using kha.graphics2.GraphicsExtension;
#end

class KCirclePainter extends KPainter {
	public function new(g4: kha.graphics4.Graphics, canvas: Canvas) {
		final quadUniformNames = new Vector(QuadUniforms._numUniforms);
		quadUniformNames[QuadUniforms.circles] = "circles";

		final styleUniformNames = new Vector(StyleUniforms._numUniforms);
		styleUniformNames[StyleUniforms.borderColors] = "borderColors";
		styleUniformNames[StyleUniforms.shadowColors] = "shadowColors";
		styleUniformNames[StyleUniforms.attributes] = "attributes";

		super(g4, canvas, quadUniformNames, styleUniformNames, Shaders.rect_vert, Shaders.circle_frag);
	}

	#if (KOUI_EFFECTS_OFF || KOUI_EFFECTS_SHADOW_OFF)
	private static inline override function init() {}

	@:access(koui.elements.Element)
	public static inline function fillKCircle(
			cx: FastFloat, cy: FastFloat, radius: FastFloat,
			styleBorder: Dynamic, styleGradient: Dynamic, styleShadow: Dynamic) {
		g2.color = styleGradient.colorBottomRight;
		g2.fillCircle(cx, cy, radius);
	}
	#else

	@:access(koui.elements.Element)
	public function fillKCircle(cx: FastFloat, cy: FastFloat, radius: FastFloat, style: Style) {
		// Draw buffer if full
		if (quadIndex + 1 >= KPainter.MAX_QUADS_PER_DRAWCALL || nextFreeStyleIndex + 1 >= KPainter.MAX_STYLES_PER_DRAWCALL) {
			drawBuffer();
		}

		var canvasW = canvas.width;
		var canvasH = canvas.height;

		var styleBorder = style.border;
		var styleColor = style.color;
		var styleShadow = style.dropShadow;
		var shadowWidth = styleShadow.radius;

		// Calculate shadow quad positions in window space
		var posLeft = (cx - radius - shadowWidth) / canvasW;
		var posRight = (cx + radius + shadowWidth) / canvasW;
		// Todo: use Image.renderTargetInvertedY
		#if (kha_opengl || kha_webgl)
		var posTop = ((cy - radius - shadowWidth)) / canvasH;
		var posBottom = ((cy + radius + shadowWidth)) / canvasH;
		#else
		// Invert y axis
		var posTop = (canvasH - (cy - radius - shadowWidth)) / canvasH;
		var posBottom = (canvasH - (cy + radius + shadowWidth)) / canvasH;
		#end

		// Map from [0, 1] range to [-1, 1]
		posLeft = posLeft * 2 - 1;
		posRight = posRight * 2 - 1;
		posTop = posTop * 2 - 1;
		posBottom = posBottom * 2 - 1;

		var colorTopLeft = styleColor.bg;
		var colorBottomRight = styleColor.bg;
		var direction = true;
		if (styleColor.useGradient) {
			colorTopLeft = styleColor.gradient.colorTopLeft;
			colorBottomRight = styleColor.gradient.colorBottomRight;
			direction = styleColor.gradient.direction;
		}

		final styleIndex = getStyleIndex(style);

		setRectVertices(posLeft, posRight, posTop, posBottom, styleIndex);
		setRectColors(direction, colorTopLeft, colorBottomRight, style.opacity);

		quadUniformValues[QuadUniforms.circles][quadIndex * 4 + 0] = cx;
		quadUniformValues[QuadUniforms.circles][quadIndex * 4 + 1] = cy;
		quadUniformValues[QuadUniforms.circles][quadIndex * 4 + 2] = radius;
		quadUniformValues[QuadUniforms.circles][quadIndex * 4 + 3] = 0; // Unused

		// Required to implicitly cast the dynamic value to kha.Color
		var borderColor: kha.Color = styleBorder.color;
		styleUniformValues[StyleUniforms.borderColors][styleIndex * 4 + 0] = borderColor.R;
		styleUniformValues[StyleUniforms.borderColors][styleIndex * 4 + 1] = borderColor.G;
		styleUniformValues[StyleUniforms.borderColors][styleIndex * 4 + 2] = borderColor.B;
		styleUniformValues[StyleUniforms.borderColors][styleIndex * 4 + 3] = borderColor.A * style.opacity;

		var shadowColor: kha.Color = styleShadow.color;
		if (styleShadow.radius == 0) shadowColor = kha.Color.Transparent;
		styleUniformValues[StyleUniforms.shadowColors][styleIndex * 4 + 0] = shadowColor.R;
		styleUniformValues[StyleUniforms.shadowColors][styleIndex * 4 + 1] = shadowColor.G;
		styleUniformValues[StyleUniforms.shadowColors][styleIndex * 4 + 2] = shadowColor.B;
		styleUniformValues[StyleUniforms.shadowColors][styleIndex * 4 + 3] = shadowColor.A * style.opacity;

		styleUniformValues[StyleUniforms.attributes][styleIndex * 4 + 0] = styleBorder.size;
		// Todo: Rename shadow radius to width
		styleUniformValues[StyleUniforms.attributes][styleIndex * 4 + 1] = styleShadow.radius;
		styleUniformValues[StyleUniforms.attributes][styleIndex * 4 + 2] = styleShadow.falloff;
		styleUniformValues[StyleUniforms.attributes][styleIndex * 4 + 3] = 0; // Unused

		quadIndex++;
	}
	#end
}

private enum abstract QuadUniforms(Int) to Int {
	var circles;

	var _numUniforms;
}

private enum abstract StyleUniforms(Int) to Int {
	var borderColors;
	var shadowColors;
	var attributes;

	var _numUniforms;
}
