package koui.graphics;

import haxe.ds.Vector;

import kha.Canvas;
import kha.FastFloat;
import kha.arrays.Float32Array;
import kha.graphics4.ConstantLocation;
import kha.graphics4.FragmentShader;
import kha.graphics4.Graphics;
import kha.graphics4.Graphics2.PipelineCache;
import kha.graphics4.Graphics2.SimplePipelineCache;
import kha.graphics4.IndexBuffer;
import kha.graphics4.PipelineState;
import kha.graphics4.Usage;
import kha.graphics4.VertexBuffer;
import kha.graphics4.VertexData;
import kha.graphics4.VertexShader;
import kha.graphics4.VertexStructure;

import koui.Koui;
import koui.theme.Style;

using StringTools;

class KPainter {
	#if (KOUI_EFFECTS_OFF || KOUI_EFFECTS_SHADOW_OFF)
	public function new(g4: kha.graphics4.Graphics, quadUniformNames: Array<String>) {}
	private static inline override function init() {}
	#else

	public var g4: Graphics;
	public var pipelineCache: PipelineCache;
	var canvas: Canvas;
	var pipeline: PipelineState;
	var vert: VertexShader;
	var frag: FragmentShader;
	var indexBuffer: IndexBuffer;
	var vertexBuffers: Array<VertexBuffer>;
	var structures: Array<VertexStructure>;
	var structureLengths: Vector<Int>;
	// vertexBuffer contents
	var vCoords: Float32Array;
	var vColors: Float32Array;

	static final MAX_QUADS_PER_DRAWCALL = 128;
	static final MAX_STYLES_PER_DRAWCALL = 16;
	var quadIndex = 0;
	var nextFreeStyleIndex = 0;
	final styleIndices = new Map<Style, Int>();

	// Uniforms
	final quadUniformNames: Vector<String>;
	final quadUniformLocations: Array<ConstantLocation>;
	final quadUniformValues: Array<Float32Array>;
	final styleUniformNames: Vector<String>;
	final styleUniformLocations: Array<ConstantLocation>;
	final styleUniformValues: Array<Float32Array>;

	public function new(
		g4: kha.graphics4.Graphics, canvas: Canvas,
		quadUniformNames: Vector<String>, styleUniformNames: Vector<String>,
		vert: VertexShader, frag: FragmentShader
	) {
		this.g4 = g4;
		this.canvas = canvas;
		this.canvas = canvas;
		this.quadUniformNames = quadUniformNames;
		this.styleUniformNames = styleUniformNames;
		this.vert = vert;
		this.frag = frag;

		quadUniformValues = new Array();
		quadUniformValues.resize(getNumQuadUniforms());
		quadUniformLocations = new Array();
		quadUniformLocations.resize(getNumQuadUniforms());

		styleUniformValues = new Array();
		styleUniformValues.resize(getNumQuadUniforms());
		styleUniformLocations = new Array();
		styleUniformLocations.resize(getNumQuadUniforms());
	}

	public function init() {
		quadIndex = 0;

		final numQuadUniforms = getNumQuadUniforms();
		final valuesPerQuadUniform = MAX_QUADS_PER_DRAWCALL * 4;
		final quadUniformRawValues = new Float32Array(numQuadUniforms * valuesPerQuadUniform /* vec4 */);
		var offset = 0;
		for (i in 0...numQuadUniforms) {
			quadUniformValues[i] = quadUniformRawValues.subarray(offset, offset += valuesPerQuadUniform);
		}

		final numStyleUniforms = getNumStyleUniforms();
		final valuesPerStyleUniform = MAX_STYLES_PER_DRAWCALL * 4;
		final styleUniformRawValues = new Float32Array(numStyleUniforms * valuesPerStyleUniform /* vec4 */);
		offset = 0;
		for (i in 0...numStyleUniforms) {
			styleUniformValues[i] = styleUniformRawValues.subarray(offset, offset += valuesPerStyleUniform);
		}

		if (structures == null) {
			initVertexStructures();
		}
		if (indexBuffer == null || vertexBuffers == null) {
			initBuffers();
		}
		if (pipeline == null) {
			initPipeline();
		}
	}

	inline function getNumQuadUniforms(): Int {
		return quadUniformNames.length;
	}

	inline function getNumStyleUniforms(): Int {
		return styleUniformNames.length;
	}

	function getStyleIndex(style: Style): Int {
		var styleIndex = styleIndices[style];
		if (styleIndex == null) {
			styleIndex = nextFreeStyleIndex;
			styleIndices[style] = styleIndex;
			nextFreeStyleIndex++;
		}
		return styleIndex;
	}

	function initVertexStructures() {
		structures = new Array();
		structureLengths = new Vector(2);

		// Vertex position
		structures[0] = new VertexStructure();
		structures[0].add("pos", VertexData.Float3);
		// if (g4.instancedRenderingAvailable()) structures[0].instanced = true;

		// Vertex color
		structures[1] = new VertexStructure();
		structures[1].add("col", VertexData.Float4);
		// if (g4.instancedRenderingAvailable()) structures[1].instanced = true;

		structureLengths[0] = 3;
		structureLengths[1] = 4;
	}

	function initPipeline() {
		pipeline = new PipelineState();
		pipeline.inputLayout = structures;
		pipeline.vertexShader = vert;
		pipeline.fragmentShader = frag;

		pipeline.blendSource = SourceAlpha;
		pipeline.blendDestination = InverseSourceAlpha;
		pipeline.blendOperation = Add;
		pipeline.alphaBlendOperation = Add;
		pipeline.alphaBlendSource = BlendOne;
		pipeline.alphaBlendDestination = InverseSourceAlpha;
		pipeline.cullMode = None;

		pipeline.compile();

		pipelineCache = new SimplePipelineCache(pipeline, false);

		for (i in 0...getNumQuadUniforms()) {
			quadUniformLocations[i] = pipeline.getConstantLocation(quadUniformNames[i]);
		}

		for (i in 0...getNumStyleUniforms()) {
			styleUniformLocations[i] = pipeline.getConstantLocation(styleUniformNames[i]);
		}
	}

	function initBuffers() {
		vertexBuffers = new Array();

		final maxNumVerts = MAX_QUADS_PER_DRAWCALL * 4;
		vertexBuffers[0] = new VertexBuffer(maxNumVerts, structures[0], Usage.DynamicUsage);
		vertexBuffers[1] = new VertexBuffer(maxNumVerts, structures[1], Usage.DynamicUsage);
		vCoords = new Float32Array(maxNumVerts * structureLengths[0]);
		vColors = new Float32Array(maxNumVerts * structureLengths[1]);

		var indices = [0, 1, 2, 1, 2, 3];
		var indicesLength = indices.length;

		indexBuffer = new IndexBuffer(MAX_QUADS_PER_DRAWCALL * indicesLength, Usage.StaticUsage);
		var ibuffer = indexBuffer.lock();
		for (i in 0...MAX_QUADS_PER_DRAWCALL) {
			for (j in 0...indicesLength) {
				ibuffer[i * indicesLength + j] = i * 4 + indices[j]; // 4 = Quad
			}
		}
		indexBuffer.unlock();
	}

	inline function setRectVertices(left: FastFloat, right: FastFloat, top: FastFloat, bottom: FastFloat, styleIndex: Int) {
		var quadOffset = quadIndex * structureLengths[0] * 4; // 4 vertices

		vCoords.set(quadOffset + 0, left);
		vCoords.set(quadOffset + 1, top);
		// Hack: use z coordinate as instanceID because we don't use instanced
		// rendering
		vCoords.set(quadOffset + 2, quadIndex << 8 | styleIndex);

		vCoords.set(quadOffset + 3, left);
		vCoords.set(quadOffset + 4, bottom);
		vCoords.set(quadOffset + 5, quadIndex << 8 | styleIndex);

		vCoords.set(quadOffset + 6, right);
		vCoords.set(quadOffset + 7, top);
		vCoords.set(quadOffset + 8, quadIndex << 8 | styleIndex);

		vCoords.set(quadOffset + 9, right);
		vCoords.set(quadOffset + 10, bottom);
		vCoords.set(quadOffset + 11, quadIndex << 8 | styleIndex);
	}

	inline function setRectColors(direction: Bool, colorTopLeft: kha.Color, colorBottomRight: kha.Color, opacity: FastFloat) {
		var colorTL: kha.Color;
		var colorTR: kha.Color;
		var colorBL: kha.Color;
		var colorBR: kha.Color;

		// Top->Down
		if (direction) {
			colorTL = colorTopLeft;
			colorTR = colorTopLeft;
			colorBL = colorBottomRight;
			colorBR = colorBottomRight;
		}
		// Left->Right
		else {
			colorTL = colorTopLeft;
			colorTR = colorBottomRight;
			colorBL = colorTopLeft;
			colorBR = colorBottomRight;
		}

		var quadOffset = quadIndex * structureLengths[1] * 4;

		vColors.set(quadOffset + 0, colorTL.R);
		vColors.set(quadOffset + 1, colorTL.G);
		vColors.set(quadOffset + 2, colorTL.B);
		vColors.set(quadOffset + 3, colorTL.A * opacity);

		vColors.set(quadOffset + 4, colorBL.R);
		vColors.set(quadOffset + 5, colorBL.G);
		vColors.set(quadOffset + 6, colorBL.B);
		vColors.set(quadOffset + 7, colorBL.A * opacity);

		vColors.set(quadOffset + 8, colorTR.R);
		vColors.set(quadOffset + 9, colorTR.G);
		vColors.set(quadOffset + 10, colorTR.B);
		vColors.set(quadOffset + 11, colorTR.A * opacity);

		vColors.set(quadOffset + 12, colorBR.R);
		vColors.set(quadOffset + 13, colorBR.G);
		vColors.set(quadOffset + 14, colorBR.B);
		vColors.set(quadOffset + 15, colorBR.A * opacity);
	}

	function drawBuffer() {
		var vbCoords = vertexBuffers[0].lock();
		for (i in 0...vCoords.length) {
			vbCoords.set(i, vCoords[i]);
		}
		vertexBuffers[0].unlock();
		var vbColors = vertexBuffers[1].lock();
		for (i in 0...vColors.length) {
			vbColors.set(i, vColors[i]);
		}
		vertexBuffers[1].unlock();

		g4.setPipeline(pipeline);
		g4.setIndexBuffer(indexBuffer);
		g4.setVertexBuffers(vertexBuffers);

		for (i in 0...getNumQuadUniforms()) {
			g4.setFloats(quadUniformLocations[i], quadUniformValues[i]);
		}
		for (i in 0...getNumStyleUniforms()) {
			g4.setFloats(styleUniformLocations[i], styleUniformValues[i]);
		}

		// if (g4.instancedRenderingAvailable()) {
		// 	// 6 = 2 * 3 vertices
		// 	g4.drawIndexedVerticesInstanced(quadIndex, 0, quadIndex * 6);
		// } else {
		g4.drawIndexedVertices(0, quadIndex * 6);

		#if KOUI_DEBUG_DRAWINGTIME
		Koui.numDrawCalls++;
		Koui.bufferSizes.push(quadIndex);
		#end
		// }

		quadIndex = 0;
		nextFreeStyleIndex = 0;
		styleIndices.clear();
	}

	public function end() {
		if (quadIndex > 0) drawBuffer();
	}
	#end
}
