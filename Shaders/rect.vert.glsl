#version 450

in vec3 pos;
in vec4 col;

out vec4 color;
flat out int instanceID;
flat out int styleID;

void main() {
	color = col;
	const int instStyleIDs = int(pos.z);
	instanceID = instStyleIDs >> 8;
	styleID = instStyleIDs & 0xFF;
	gl_Position = vec4(pos.x, pos.y, 0.0, 1.0);
}
