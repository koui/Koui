<div align="center">
    <a href="https://koui.gitlab.io">
        <img title="Koui Logo" src=".gitlab/koui_header.png" alt="Koui Logo" data-align="inline" height="200px">
    </a>
</div>
<div align="center">
    <a href="https://gitlab.com/koui/Koui/-/wikis/home">
        <img src="https://img.shields.io/badge/Wiki-Docs-orange?style=for-the-badge&logo=gitlab"/>
    </a>
    <a href="https://koui.gitlab.io">
        <img src="https://img.shields.io/website?style=for-the-badge&url=https%3A%2F%2Fkoui.gitlab.io%2F"/>
    </a>
    <a href="https://gitlab.com/koui/Koui/pipelines">
        <img src="https://img.shields.io/gitlab/pipeline/koui/Koui?label=Pipeline&logo=gitlab&style=for-the-badge"/>
    </a>
    <a href="https://gitlab.com/koui/Koui/-/releases">
        <img src="https://img.shields.io/static/v1?label=RElease&message=2020.10&color=blue&style=for-the-badge"/>
    </a>
    <a href="https://gitlab.com/koui/Koui/-/blob/master/LICENSE.md">
        <img src="https://img.shields.io/static/v1?label=License&message=zlib&color=blue&style=for-the-badge"/>
    </a>
</div>
<br>
<div align="center">
    <p><strong>Koui</strong> is a very fast and flexible UI library for the [Kha](https://github.com/Kode/Kha) framework.<br>
    It's free and open source, but if you want to support me and my work, you can [buy it at Gumroad](https://gumroad.com/products/WQzWn).</p>
</div>
<div align="center">
    <a href="https://gitlab.com/koui/Koui/-/wikis/Getting-Started">
        <img width=190vh src="https://img.shields.io/badge/Get Started-blue?style=for-the-badge"></img>
    </a>
</div>
<br>
<br>
<div align="center">
    ![Screenshot](.gitlab/screenshot.png)
    <p>UI design provided by Sebastian Becker. Follow him on [Behance](https://www.behance.net/sebbeck) and [Instagram](https://www.instagram.com/sebastian_becker93/)!</p>
</div>


# Getting started

See [Wiki/Getting Started](https://gitlab.com/koui/Koui/-/wikis/Getting-Started) for detailed instructions on how to install Koui and first steps.

# Planned features

Some of the bigger features that are planned but not yet implemented:

- Animations
- Controller and touch support
- Logic node pack for Armory3D. Currently working on this at the [Koui Logic Nodes](https://gitlab.com/koui/koui-logic-nodes) repository.

If you want to contribute, feel free to do so! Any help is highly appreciated!

# License

Koui is licensed under the [zlib license](LICENSE.md).

This project uses [Montserrat](https://github.com/JulietaUla/Montserrat) as the default font for the user interface, which is licensed under the [SIL Open Font License 1.1](https://gitlab.com/koui/Koui/-/blob/master/Assets/Montserrat-License-OFL.txt).

> If you don't agree with the font's license for your own projects, please remove that font and its license file from your local copy of Koui and remove the reference to it from Koui's `khafile.js`.
>
> Koui will not work until you load in another font and reference it in the theme file.
